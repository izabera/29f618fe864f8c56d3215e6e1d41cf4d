#include <stdint.h>
#include <sys/types.h>

//              "01234567890123456789"
#define padding "                    "
#define numbers "00000000000000000000"

__attribute__((noinline))
ssize_t printaddr_(char *restrict str, const void *restrict ptr) {
  ssize_t len = sizeof padding ": 0x" numbers "\n" - 1;

  char xdigits[16] = "0123456789abcdef",
       *s = str + len - 1;

  for (uintptr_t x = (uintptr_t) ptr; x; x >>= 4)
    *--s = xdigits[x&15];

  asm volatile("syscall"
               :
               "=a"(len)      // return in ax
               :
               "a"(1),        // ax = write
               "D"(1),        // di = fd
               "S"(str),      // si = str
               "d"(len)       // dx = len bytes
               :
               "r11", "rcx"   // trashed by syscall and sysret
               );
  return len;
}

// create a modifiable string with left padding
#define build(x) (sizeof #x - 1 + (char []){ padding #x ": 0x" numbers "\n" })
#define printaddr(x) printaddr_(build(x), x)

#ifndef NO_STDIO
#include <stdio.h>
#include <inttypes.h>
#endif
int main() {
  printaddr(main);
  printaddr(NULL);
  printaddr(printaddr_);
#ifndef NO_STDIO
  printf("%20s: 0x%020"PRIxPTR"\n", "main", (uintptr_t)main);
  printf("%20s: 0x%020"PRIxPTR"\n", "printaddr_", (uintptr_t)printaddr_);
#endif
}
